from edison_feeds_api import EdisonBaseAPI


class projectsController(EdisonBaseAPI):
    """Project is a set of inputs which search results are put in the same database index."""

    _controller_name = "projectsController"

    def add(self, projectName, **kwargs):
        """Create new projects for authenticated user.

        Args:
            isRunning: (boolean): isRunning
            projectName: (string): projectName
            textProcessingPipelineId: (string): If set empty string, the default pipeline will be used.
            ttl: (string): Time To Live.For documents in Elasticsearch.Allows you to specify the minimum time a document should live,after which time the document is deleted automatically.Empty string or null means unlimited time.Example: "1y", "3M", "3w", "1d".The supported time units are: y(year), M(month), w(week), d(day).

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['projectName', '**kwargs']
        parameters_names_map = {}
        api = '/projects/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def clone(self, newProjectName, projectId):
        """Duplicate project to new name.

        Args:
            newProjectName: (string): newProjectName
            projectId: (string): projectId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['newProjectName', 'projectId']
        parameters_names_map = {}
        api = '/projects/clone'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, projectId, **kwargs):
        """Edit existing projects by ID.

        Args:
            isRunning: (boolean): isRunning
            name: (string): name
            projectId: (string): projectId
            textProcessingPipelineId: (string): If set empty string, the default pipeline will be used.
            ttl: (string): ttl

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['projectId', '**kwargs']
        parameters_names_map = {}
        api = '/projects/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """Return the list of projects for authenticated user."""

        api_parameters = None
        parameters_names_map = {}
        api = '/projects/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def processFile(self, file, privacyMode, projectId, **kwargs):
        """Process the text from file. Supported file formats:  - https://tika.apache.org/1.13/formats.html - .tif, .bmp, .jpg, .png

        Args:
            date: (integer): The number of seconds since January 1, 1970, 00:00:00 GMT.
            file: (file): file
            languageId: (string): empty - AutoDetect.
            logging: (boolean): logging
            office365EmailType: (string): office365EmailType
            office365EmailsIncludeMode: (string): office365EmailsIncludeMode
            office365Groups: (array): office365Groups
            office365MailFolder: (string): office365MailFolder
            privacyMode: (boolean): In this mode the processed text not saved.
            projectId: (string): projectId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['file', 'privacyMode', 'projectId', '**kwargs']
        parameters_names_map = {}
        api = '/projects/processFile'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def processReview(self, privacyMode, productId, productName, projectId, reviewTypeId, **kwargs):
        """Process the review.

        Args:
            author: (string): Person who created the review.
            businessCategories: (array): Business categories.
            businessCoordinatesLatitude: (number): Business coordinates latitude.
            businessCoordinatesLongitude: (number): Business coordinates longitude.
            businessId: (string): Business id.
            businessLocationAddress1: (string): Business location address1.
            businessLocationAddress2: (string): Business location address2.
            businessLocationAddress3: (string): Business location address3.
            businessLocationCity: (string): Business location city.
            businessLocationCountry: (string): Business location country.
            businessLocationState: (string): Business location state.
            businessLocationZipCode: (string): Business location ZIP code.
            businessName: (string): Business name.
            businessPhone: (string): Business phone.
            businessPrice: (integer): Business price.
            businessRating: (number): Business rating.
            businessTransactions: (array): Business transactions.
            businessUrl: (string): Business url.
            checkinCount: (integer): Check-in count.
            coolCount: (integer): Cool count.
            date: (integer): Date the review was posted. The number of seconds since January 1, 1970, 00:00:00 GMT.
            funnyCount: (integer): Funny count.
            helpfulCount: (integer): The number of people that indicated this review was helpful.
            languageId: (string): empty - AutoDetect.
            logging: (boolean): logging
            notHelpfulCount: (integer): The number of people that indicated this review was not helpful.
            options: (array): List of possible options.
            privacyMode: (boolean): In this mode the processed text not saved.
            productId: (string): The product id of the item being reviews.
            productName: (string): The product name.
            projectId: (string): API id of project.
            rating: (number): The review star rating.
            recommended: (boolean): Recommended.
            review: (string): Text of the review.
            reviewTypeId: (string): Amazon, Home depot, Walmart, etc...
            reviewVotesCount: (integer): Review votes count.
            url: (string): Review url.
            verifiedUser: (boolean): Verified user.

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['privacyMode', 'productId', 'productName', 'projectId', 'reviewTypeId', '**kwargs']
        parameters_names_map = {}
        api = '/projects/processReview'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def processText(self, privacyMode, projectId, text, **kwargs):
        """Process the text.

        Args:
            date: (integer): The number of seconds since January 1, 1970, 00:00:00 GMT.
            filterFields: (string): filterFields
            languageId: (string): empty - AutoDetect.
            logging: (boolean): logging
            office365EmailType: (string): office365EmailType
            office365EmailsIncludeMode: (string): office365EmailsIncludeMode
            office365Groups: (array): office365Groups
            office365MailFolder: (string): office365MailFolder
            privacyMode: (boolean): In this mode the processed text not saved.
            projectId: (string): projectId
            text: (string): text

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['privacyMode', 'projectId', 'text', '**kwargs']
        parameters_names_map = {}
        api = '/projects/processText'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, projectId):
        """Remove existing projects for authenticated user.

        Args:
            projectId: (string): projectId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['projectId']
        parameters_names_map = {}
        api = '/projects/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, projectId):
        """Return the project with specified ID.

        Args:
            projectId: (string): projectId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['projectId']
        parameters_names_map = {}
        api = '/projects/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def status(self, id, isRunning):
        """Set existing projects status.

        Args:
            id: (string): id
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id', 'isRunning']
        parameters_names_map = {}
        api = '/projects/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
