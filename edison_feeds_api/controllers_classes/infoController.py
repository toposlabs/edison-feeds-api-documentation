from edison_feeds_api import EdisonBaseAPI


class infoController(EdisonBaseAPI):
    """Info Controller"""

    _controller_name = "infoController"

    def retrieve(self):
        """Return info."""

        api_parameters = None
        parameters_names_map = {}
        api = '/info/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
