from edison_feeds_api import EdisonBaseAPI


class inputsGroupsController(EdisonBaseAPI):
    """InputGroups is a manager for inputs."""

    _controller_name = "inputsGroupsController"

    def add(self, name, projectId, **kwargs):
        """Create new inputsGroup for project.

        Args:
            description: (string): Description. Any text.
            isRunning: (boolean): isRunning
            name: (string): name
            projectId: (string): projectId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['name', 'projectId', '**kwargs']
        parameters_names_map = {}
        api = '/inputsGroups/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, inputsGroupId, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            description: (string): Description. Any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            name: (string): name

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId', '**kwargs']
        parameters_names_map = {}
        api = '/inputsGroups/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, projectId):
        """Return the list of inputsGroups for project.

        Args:
            projectId: (string): projectId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['projectId']
        parameters_names_map = {}
        api = '/inputsGroups/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, inputsGroupId):
        """Remove existing inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputsGroups/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, inputsGroupId):
        """Return the inputsGroup with specified ID.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputsGroups/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def status(self, inputsGroupId, isRunning):
        """Set status for existing inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId', 'isRunning']
        parameters_names_map = {}
        api = '/inputsGroups/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
