from edison_feeds_api import EdisonBaseAPI


class taskTypesController(EdisonBaseAPI):
    """Task Types Controller"""

    _controller_name = "taskTypesController"

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/task-types/list'
        actions = ['get']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, id):
        """

        Args:
            id: (string): id

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id']
        parameters_names_map = {}
        api = '/task-types/retrieve'
        actions = ['get']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
