from edison_feeds_api import EdisonBaseAPI


class themesController(EdisonBaseAPI):
    """Themes"""

    _controller_name = "themesController"

    def list(self):
        """Return list of themes."""

        api_parameters = None
        parameters_names_map = {}
        api = '/themes/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, themeId):
        """Retrieve existing theme.

        Args:
            themeId: (string): themeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeId']
        parameters_names_map = {}
        api = '/themes/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
