from edison_feeds_api import EdisonBaseAPI


class processReviewTasksController(EdisonBaseAPI):
    """Process Review Tasks Controller"""

    _controller_name = "processReviewTasksController"

    def submit(self, privacyMode, productId, productName, projectId, review, reviewTypeId, **kwargs):
        """

        Args:
            author: (string): Person who created the review.
            businessCategories: (array): Business categories.
            businessCoordinatesLatitude: (number): Business coordinates latitude.
            businessCoordinatesLongitude: (number): Business coordinates longitude.
            businessId: (string): Business id.
            businessLocationAddress1: (string): Business location address1.
            businessLocationAddress2: (string): Business location address2.
            businessLocationAddress3: (string): Business location address3.
            businessLocationCity: (string): Business location city.
            businessLocationCountry: (string): Business location country.
            businessLocationState: (string): Business location state.
            businessLocationZipCode: (string): Business location ZIP code.
            businessName: (string): Business name.
            businessPhone: (string): Business phone.
            businessPrice: (integer): Business price.
            businessRating: (number): Business rating.
            businessTransactions: (array): Business transactions.
            businessUrl: (string): Business url.
            checkinCount: (integer): Check-in count.
            coolCount: (integer): Cool count.
            date: (integer): Date the review was posted. The number of seconds since January 1, 1970, 00:00:00 GMT.
            funnyCount: (integer): Funny count.
            helpfulCount: (integer): The number of people that indicated this review was helpful.
            languageId: (string): empty - AutoDetect.
            logging: (boolean): logging
            notHelpfulCount: (integer): The number of people that indicated this review was not helpful.
            options: (array): List of possible options.
            privacyMode: (boolean): In this mode the processed text not saved.
            productId: (string): The product id of the item being reviews.
            productName: (string): The product name.
            projectId: (string): API id of project.
            rating: (number): The review star rating.
            recommended: (boolean): Recommended.
            review: (string): Text of the review.
            reviewTypeId: (string): Amazon, Home depot, Walmart, etc...
            reviewVotesCount: (integer): Review votes count.
            url: (string): Review url.
            verifiedUser: (boolean): Verified user.

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['privacyMode', 'productId', 'productName', 'projectId', 'review', 'reviewTypeId', '**kwargs']
        parameters_names_map = {}
        api = '/process-review-tasks/submit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
