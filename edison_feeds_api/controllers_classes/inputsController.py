from edison_feeds_api import EdisonBaseAPI


class inputsController(EdisonBaseAPI):
    """Inputs management."""

    _controller_name = "inputsController"

    def amazonProductReviewsAdd(self, inputName, inputsGroupId, **kwargs):
        """Create new input for inputsGroup.

        Args:
            amazonProductId: (string): Amazon product id for collecting reviews. Accept list of id's separated by commas
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/amazonProductReviews/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def amazonProductReviewsEdit(self, inputId, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            amazonProductId: (string): Amazon product id for collecting reviews. Accept list of id's separated by commas
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/amazonProductReviews/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def amazonProductReviewsList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/amazonProductReviews/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def amazonProductReviewsRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/amazonProductReviews/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def amazonProductReviewsRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/amazonProductReviews/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def amazonProductReviewsStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/amazonProductReviews/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistCommunityAdd(self, inputName, inputsGroupId, locationKey, **kwargs):
        """Create new input for inputsGroup.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            description: (string): description
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            milesFromZip: (integer): milesFromZip
            postedToday: (boolean): postedToday
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/community/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistCommunityEdit(self, inputId, locationKey, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            description: (string): description
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            milesFromZip: (integer): milesFromZip
            postedToday: (boolean): postedToday
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/community/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistCommunityList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/craigslist/community/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistCommunityRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/community/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistCommunityRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/community/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistCommunityStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/craigslist/community/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistEventsAdd(self, inputName, inputsGroupId, locationKey, **kwargs):
        """Create new input for inputsGroup.

        Args:
            artFilm: (boolean): artFilm
            bundleDuplicates: (boolean): bundleDuplicates
            career: (boolean): career
            charitable: (boolean): charitable
            competition: (boolean): competition
            dance: (boolean): dance
            description: (string): description
            festFair: (boolean): festFair
            fitnessHealth: (boolean): fitnessHealth
            foodDrink: (boolean): foodDrink
            free: (boolean): free
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            kidFriendly: (boolean): kidFriendly
            literary: (boolean): literary
            locationKey: (string): locationKey
            music: (boolean): music
            outdoor: (boolean): outdoor
            postedToday: (boolean): postedToday
            query: (string): query
            sale: (boolean): sale
            searchTitlesOnly: (boolean): searchTitlesOnly
            singles: (boolean): singles
            tech: (boolean): tech

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/events/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistEventsEdit(self, inputId, locationKey, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            artFilm: (boolean): artFilm
            bundleDuplicates: (boolean): bundleDuplicates
            career: (boolean): career
            charitable: (boolean): charitable
            competition: (boolean): competition
            dance: (boolean): dance
            description: (string): description
            festFair: (boolean): festFair
            fitnessHealth: (boolean): fitnessHealth
            foodDrink: (boolean): foodDrink
            free: (boolean): free
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            kidFriendly: (boolean): kidFriendly
            literary: (boolean): literary
            locationKey: (string): locationKey
            music: (boolean): music
            outdoor: (boolean): outdoor
            postedToday: (boolean): postedToday
            query: (string): query
            sale: (boolean): sale
            searchTitlesOnly: (boolean): searchTitlesOnly
            singles: (boolean): singles
            tech: (boolean): tech

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/events/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistEventsList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/craigslist/events/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistEventsRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/events/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistEventsRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/events/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistEventsStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/craigslist/events/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistForsaleAdd(self, inputName, inputsGroupId, locationKey, **kwargs):
        """Create new input for inputsGroup.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            conditionExcellent: (boolean): conditionExcellent
            conditionFair: (boolean): conditionFair
            conditionGood: (boolean): conditionGood
            conditionLikeNew: (boolean): conditionLikeNew
            conditionNew: (boolean): conditionNew
            conditionSalvage: (boolean): conditionSalvage
            cryptocurrencyOk: (boolean): cryptocurrencyOk
            description: (string): description
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            makeAndModel: (string): makeAndModel
            milesFromZip: (integer): milesFromZip
            modelYearMax: (integer): modelYearMax
            modelYearMin: (integer): modelYearMin
            odometerMax: (integer): odometerMax
            odometerMin: (integer): odometerMin
            postedToday: (boolean): postedToday
            priceMax: (integer): priceMax
            priceMin: (integer): priceMin
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/forSale/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistForsaleEdit(self, inputId, locationKey, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            conditionExcellent: (boolean): conditionExcellent
            conditionFair: (boolean): conditionFair
            conditionGood: (boolean): conditionGood
            conditionLikeNew: (boolean): conditionLikeNew
            conditionNew: (boolean): conditionNew
            conditionSalvage: (boolean): conditionSalvage
            cryptocurrencyOk: (boolean): cryptocurrencyOk
            description: (string): description
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            makeAndModel: (string): makeAndModel
            milesFromZip: (integer): milesFromZip
            modelYearMax: (integer): modelYearMax
            modelYearMin: (integer): modelYearMin
            odometerMax: (integer): odometerMax
            odometerMin: (integer): odometerMin
            postedToday: (boolean): postedToday
            priceMax: (integer): priceMax
            priceMin: (integer): priceMin
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/forSale/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistForsaleList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/craigslist/forSale/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistForsaleRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/forSale/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistForsaleRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/forSale/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistForsaleStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/craigslist/forSale/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistGigsAdd(self, inputName, inputsGroupId, locationKey, **kwargs):
        """Create new input for inputsGroup.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            description: (string): description
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isPaid: (boolean): isPaid
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            milesFromZip: (integer): milesFromZip
            postedToday: (boolean): postedToday
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/gigs/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistGigsEdit(self, inputId, locationKey, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            description: (string): description
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputId: (string): inputId
            isPaid: (boolean): isPaid
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            milesFromZip: (integer): milesFromZip
            postedToday: (boolean): postedToday
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/gigs/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistGigsList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/craigslist/gigs/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistGigsRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/gigs/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistGigsRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/gigs/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistGigsStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/craigslist/gigs/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistHousingAdd(self, inputName, inputsGroupId, locationKey, **kwargs):
        """Create new input for inputsGroup.

        Args:
            bathroomsMax: (integer): bathroomsMax
            bathroomsMin: (integer): bathroomsMin
            bedroomsMax: (integer): bedroomsMax
            bedroomsMin: (integer): bedroomsMin
            bundleDuplicates: (boolean): bundleDuplicates
            catsOk: (boolean): catsOk
            description: (string): description
            dogsOk: (boolean): dogsOk
            ft2Max: (integer): ft2Max
            ft2Min: (integer): ft2Min
            furnished: (boolean): furnished
            hasImage: (boolean): hasImage
            housingTypeApartment: (boolean): housingTypeApartment
            housingTypeAssistedLiving: (boolean): housingTypeAssistedLiving
            housingTypeCondo: (boolean): housingTypeCondo
            housingTypeCottageCabin: (boolean): housingTypeCottageCabin
            housingTypeDuplex: (boolean): housingTypeDuplex
            housingTypeFlat: (boolean): housingTypeFlat
            housingTypeHouse: (boolean): housingTypeHouse
            housingTypeInLaw: (boolean): housingTypeInLaw
            housingTypeLand: (boolean): housingTypeLand
            housingTypeLoft: (boolean): housingTypeLoft
            housingTypeManufactured: (boolean): housingTypeManufactured
            housingTypeTownhouse: (boolean): housingTypeTownhouse
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            laundryLaundryInBldg: (boolean): laundryLaundryInBldg
            laundryLaundryOnSite: (boolean): laundryLaundryOnSite
            laundryNoLaundryOnSite: (boolean): laundryNoLaundryOnSite
            laundryWdHookups: (boolean): laundryWdHookups
            laundryWdInUnit: (boolean): laundryWdInUnit
            locationKey: (string): locationKey
            milesFromZip: (integer): milesFromZip
            noSmoking: (boolean): noSmoking
            parkingAttachedGarage: (boolean): parkingAttachedGarage
            parkingCarport: (boolean): parkingCarport
            parkingDetachedGarage: (boolean): parkingDetachedGarage
            parkingNoParking: (boolean): parkingNoParking
            parkingOffStreetParking: (boolean): parkingOffStreetParking
            parkingStreetParking: (boolean): parkingStreetParking
            parkingValetParking: (boolean): parkingValetParking
            postedToday: (boolean): postedToday
            priceMax: (integer): priceMax
            priceMin: (integer): priceMin
            privateBath: (boolean): privateBath
            privateRoom: (boolean): privateRoom
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            wheelchairAccess: (boolean): wheelchairAccess
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/housing/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistHousingEdit(self, inputId, locationKey, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            bathroomsMax: (integer): bathroomsMax
            bathroomsMin: (integer): bathroomsMin
            bedroomsMax: (integer): bedroomsMax
            bedroomsMin: (integer): bedroomsMin
            bundleDuplicates: (boolean): bundleDuplicates
            catsOk: (boolean): catsOk
            description: (string): description
            dogsOk: (boolean): dogsOk
            ft2Max: (integer): ft2Max
            ft2Min: (integer): ft2Min
            furnished: (boolean): furnished
            hasImage: (boolean): hasImage
            housingTypeApartment: (boolean): housingTypeApartment
            housingTypeAssistedLiving: (boolean): housingTypeAssistedLiving
            housingTypeCondo: (boolean): housingTypeCondo
            housingTypeCottageCabin: (boolean): housingTypeCottageCabin
            housingTypeDuplex: (boolean): housingTypeDuplex
            housingTypeFlat: (boolean): housingTypeFlat
            housingTypeHouse: (boolean): housingTypeHouse
            housingTypeInLaw: (boolean): housingTypeInLaw
            housingTypeLand: (boolean): housingTypeLand
            housingTypeLoft: (boolean): housingTypeLoft
            housingTypeManufactured: (boolean): housingTypeManufactured
            housingTypeTownhouse: (boolean): housingTypeTownhouse
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            laundryLaundryInBldg: (boolean): laundryLaundryInBldg
            laundryLaundryOnSite: (boolean): laundryLaundryOnSite
            laundryNoLaundryOnSite: (boolean): laundryNoLaundryOnSite
            laundryWdHookups: (boolean): laundryWdHookups
            laundryWdInUnit: (boolean): laundryWdInUnit
            locationKey: (string): locationKey
            milesFromZip: (integer): milesFromZip
            noSmoking: (boolean): noSmoking
            parkingAttachedGarage: (boolean): parkingAttachedGarage
            parkingCarport: (boolean): parkingCarport
            parkingDetachedGarage: (boolean): parkingDetachedGarage
            parkingNoParking: (boolean): parkingNoParking
            parkingOffStreetParking: (boolean): parkingOffStreetParking
            parkingStreetParking: (boolean): parkingStreetParking
            parkingValetParking: (boolean): parkingValetParking
            postedToday: (boolean): postedToday
            priceMax: (integer): priceMax
            priceMin: (integer): priceMin
            privateBath: (boolean): privateBath
            privateRoom: (boolean): privateRoom
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            wheelchairAccess: (boolean): wheelchairAccess
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/housing/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistHousingList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/craigslist/housing/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistHousingRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/housing/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistHousingRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/housing/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistHousingStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/craigslist/housing/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistJobsAdd(self, inputName, inputsGroupId, locationKey, **kwargs):
        """Create new input for inputsGroup.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            description: (string): description
            employmentTypeContract: (boolean): employmentTypeContract
            employmentTypeEmployeesChoice: (boolean): employmentTypeEmployeesChoice
            employmentTypeFullTime: (boolean): employmentTypeFullTime
            employmentTypePartTime: (boolean): employmentTypePartTime
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            internship: (boolean): internship
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            milesFromZip: (integer): milesFromZip
            nonProfit: (boolean): nonProfit
            postedToday: (boolean): postedToday
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            telecommute: (boolean): telecommute
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/jobs/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistJobsEdit(self, inputId, locationKey, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            description: (string): description
            employmentTypeContract: (boolean): employmentTypeContract
            employmentTypeEmployeesChoice: (boolean): employmentTypeEmployeesChoice
            employmentTypeFullTime: (boolean): employmentTypeFullTime
            employmentTypePartTime: (boolean): employmentTypePartTime
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputId: (string): inputId
            internship: (boolean): internship
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            milesFromZip: (integer): milesFromZip
            nonProfit: (boolean): nonProfit
            postedToday: (boolean): postedToday
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            telecommute: (boolean): telecommute
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/jobs/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistJobsList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/craigslist/jobs/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistJobsRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/jobs/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistJobsRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/jobs/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistJobsStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/craigslist/jobs/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistLocations(self):
        """Return Craigslist RSS locations list."""

        api_parameters = None
        parameters_names_map = {}
        api = '/inputs/craigslist/locations'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistResumesAdd(self, inputName, inputsGroupId, locationKey, **kwargs):
        """Create new input for inputsGroup.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            description: (string): description
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            postedToday: (boolean): postedToday
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/resumes/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistResumesEdit(self, inputId, locationKey, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            description: (string): description
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            postedToday: (boolean): postedToday
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/resumes/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistResumesList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/craigslist/resumes/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistResumesRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/resumes/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistResumesRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/resumes/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistResumesStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/craigslist/resumes/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistServicesAdd(self, inputName, inputsGroupId, locationKey, **kwargs):
        """Create new input for inputsGroup.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            description: (string): description
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            milesFromZip: (integer): milesFromZip
            postedToday: (boolean): postedToday
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/services/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistServicesEdit(self, inputId, locationKey, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            bundleDuplicates: (boolean): bundleDuplicates
            description: (string): description
            hasImage: (boolean): hasImage
            includeNearbyAreas: (boolean): includeNearbyAreas
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            locationKey: (string): locationKey
            milesFromZip: (integer): milesFromZip
            postedToday: (boolean): postedToday
            query: (string): query
            searchTitlesOnly: (boolean): searchTitlesOnly
            zipCode: (string): zipCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'locationKey', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/craigslist/services/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistServicesList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/craigslist/services/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistServicesRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/services/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistServicesRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/craigslist/services/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def craigslistServicesStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/craigslist/services/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def ebaySearchAdd(self, inputName, inputsGroupId, keywords, **kwargs):
        """Create new input for inputsGroup.

        Args:
            buyerPostalCode: (string): Buyer postal code to sort.
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            keywords: (string): Keywords for search.

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'keywords', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/ebaySearch/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def ebaySearchEdit(self, inputId, keywords, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            buyerPostalCode: (string): Buyer postal code to sort.
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            keywords: (string): Keywords for search.

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'keywords', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/ebaySearch/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def ebaySearchList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/ebaySearch/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def ebaySearchRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/ebaySearch/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def ebaySearchRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/ebaySearch/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def ebaySearchStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/ebaySearch/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def homedepotProductReviewsAdd(self, inputName, inputsGroupId, **kwargs):
        """Create new input for inputsGroup.

        Args:
            description: (string): description
            homedepotProductId: (string): Home Depot product id for collecting reviews
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/homedepotProductReviews/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def homedepotProductReviewsEdit(self, inputId, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            description: (string): description
            homedepotProductId: (string): Home Depot product id for collecting reviews
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/homedepotProductReviews/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def homedepotProductReviewsList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/homedepotProductReviews/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def homedepotProductReviewsRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/homedepotProductReviews/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def homedepotProductReviewsRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/homedepotProductReviews/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def homedepotProductReviewsStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/homedepotProductReviews/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def inputTypesList(self):
        """Return the list of input types."""

        api_parameters = None
        parameters_names_map = {}
        api = '/inputs/inputTypesList'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def lowesProductReviewsAdd(self, inputName, inputsGroupId, **kwargs):
        """Create new input for inputsGroup.

        Args:
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            lowesProductUrl: (string): Lowes product url for collecting reviews

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/lowesProductReviews/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def lowesProductReviewsEdit(self, inputId, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            lowesProductUrl: (string): Lowes product url for collecting reviews

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/lowesProductReviews/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def lowesProductReviewsList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/lowesProductReviews/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def lowesProductReviewsRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/lowesProductReviews/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def lowesProductReviewsRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/lowesProductReviews/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def lowesProductReviewsStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/lowesProductReviews/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def metasearchAdd(self, category, inputName, inputsGroupId, searchQuery, **kwargs):
        """Create new input for inputsGroup.

        Args:
            category: (string): category
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            language: (string): language
            searchQuery: (string): searchQuery
            timeRange: (string): timeRange

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['category', 'inputName', 'inputsGroupId', 'searchQuery', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/metasearch/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def metasearchEdit(self, category, inputId, searchQuery, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            category: (string): category
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            language: (string): language
            searchQuery: (string): searchQuery
            timeRange: (string): timeRange

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['category', 'inputId', 'searchQuery', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/metasearch/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def metasearchList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/metasearch/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def metasearchParameters(self):
        """Return Metasearch parameters."""

        api_parameters = None
        parameters_names_map = {}
        api = '/inputs/metasearch/parameters'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def metasearchRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/metasearch/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def metasearchRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/metasearch/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def metasearchStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/metasearch/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSearchAdd(self, inputName, inputsGroupId, searchQuery, **kwargs):
        """Create new input for inputsGroup.

        Args:
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            limit: (integer): Limit.
            maxCommentsCount: (integer): Maximum amount of comments to retrieve
            maxCommentsDepth: (integer): Maximum comments depth
            searchQuery: (string): Search query.
            sort: (string): Sort.
            timeWindow: (string): Time window.

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'searchQuery', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/reddit/search/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSearchEdit(self, inputId, searchQuery, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            limit: (integer): Limit.
            maxCommentsCount: (integer): Maximum amount of comments to retrieve
            maxCommentsDepth: (integer): Maximum comments depth
            searchQuery: (string): Search query.
            sort: (string): Sort.
            timeWindow: (string): Time window.

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'searchQuery', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/reddit/search/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSearchList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/reddit/search/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSearchParameters(self):
        """Return Reddit search parameters list."""

        api_parameters = None
        parameters_names_map = {}
        api = '/inputs/reddit/search/parameters'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSearchRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/reddit/search/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSearchRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/reddit/search/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSearchStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/reddit/search/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSubredditAdd(self, inputName, inputsGroupId, **kwargs):
        """Create new input for inputsGroup.

        Args:
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            maxCommentsCount: (integer): Maximum amount of comments to retrieve
            maxCommentsDepth: (integer): Maximum comments depth
            subreddit: (string): Subreddit

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/reddit/subreddit/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSubredditEdit(self, inputId, subreddit, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            maxCommentsCount: (integer): Maximum amount of comments to retrieve
            maxCommentsDepth: (integer): Maximum comments depth
            subreddit: (string): Subreddit

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'subreddit', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/reddit/subreddit/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSubredditList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/reddit/subreddit/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSubredditRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/reddit/subreddit/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSubredditRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/reddit/subreddit/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def redditSubredditStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/reddit/subreddit/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssAdd(self, inputName, inputsGroupId, **kwargs):
        """Create new input for inputsGroup.

        Args:
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            maxRssArticleComments: (integer): Maximum amount of comments returned for each RSS article
            maxRssWatchArticleCommentsMs: (integer): Maximum amount of milliseconds to monitor comments for RSS article
            rssFeedKey: (string): rssFeedKey

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/rss/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssEdit(self, inputId, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            maxRssArticleComments: (integer): Maximum amount of comments returned for each RSS article
            maxRssWatchArticleCommentsMs: (integer): Maximum amount of milliseconds to monitor comments for RSS article
            rssFeedKey: (string): rssFeedKey

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/rss/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/rss/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssParameters(self):
        """Return RSS parameters list."""

        api_parameters = None
        parameters_names_map = {}
        api = '/inputs/rss/parameters'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/rss/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/rss/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssStatistics(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/rss/statistics'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/rss/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssBundleAdd(self, inputName, inputsGroupId, **kwargs):
        """Create new input for inputsGroup.

        Args:
            categories: (array): List of categories. Choose feeds with at least one match. Empty list processed as all possible variants.
            countries: (array): List of countries. Choose feeds with at least one match. Empty list processed as all possible variants.
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            languages: (array): List of languages. Choose feeds with at least one match. Empty list processed as all possible variants.
            maxRssArticleComments: (integer): Maximum amount of comments returned for each RSS article
            maxRssWatchArticleCommentsMs: (integer): Maximum amount of milliseconds to monitor comments for RSS article

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/rssBundle/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssBundleEdit(self, inputId, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            categories: (array): List of categories. Choose feeds with at least one match. Empty list processed as all possible variants.
            countries: (array): List of countries. Choose feeds with at least one match. Empty list processed as all possible variants.
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            languages: (array): List of languages. Choose feeds with at least one match. Empty list processed as all possible variants.
            maxRssArticleComments: (integer): Maximum amount of comments returned for each RSS article
            maxRssWatchArticleCommentsMs: (integer): Maximum amount of milliseconds to monitor comments for RSS article

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/rssBundle/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssBundleList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/rssBundle/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssBundleParameters(self):
        """Return RSS bundle parameters list."""

        api_parameters = None
        parameters_names_map = {}
        api = '/inputs/rssBundle/parameters'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssBundleRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/rssBundle/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssBundleRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/rssBundle/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssBundleStatistics(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/rssBundle/statistics'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rssBundleStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/rssBundle/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterAdd(self, inputName, inputsGroupId, **kwargs):
        """Create new input for inputsGroup.

        Args:
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            twitterTracks: (string): twitterTracks
            twitterUsers: (string): twitterUsers

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/twitter/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterEdit(self, inputId, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            twitterTracks: (string): twitterTracks
            twitterUsers: (string): twitterUsers

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/twitter/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/twitter/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/twitter/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/twitter/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterSearchAdd(self, inputName, inputsGroupId, searchQuery, **kwargs):
        """Create new input for inputsGroup.

        Args:
            description: (string): description
            geocodeLatitude: (number): geocodeLatitude
            geocodeLongitude: (number): geocodeLongitude
            geocodeRadius: (number): geocodeRadius
            geocodeUnit: (string): geocodeUnit
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            lang: (string): lang
            locale: (string): locale
            resultType: (string): resultType
            searchQuery: (string): searchQuery

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'searchQuery', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/twitter/search/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterSearchEdit(self, inputId, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            description: (string): description
            geocodeLatitude: (number): geocodeLatitude
            geocodeLongitude: (number): geocodeLongitude
            geocodeRadius: (number): geocodeRadius
            geocodeUnit: (string): geocodeUnit
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            lang: (string): lang
            locale: (string): locale
            resultType: (string): resultType
            searchQuery: (string): searchQuery

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/twitter/search/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterSearchList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/twitter/search/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterSearchParameters(self):
        """Return RSS bundle parameters list."""

        api_parameters = None
        parameters_names_map = {}
        api = '/inputs/twitter/search/parameters'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterSearchRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/twitter/search/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterSearchRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/twitter/search/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterSearchStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/twitter/search/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def twitterStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/twitter/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def websiteCrawlerAdd(self, inputName, inputsGroupId, processPagesVia, seed, **kwargs):
        """Create new input for inputsGroup.

        Args:
            acceptLanguage: (string): Custom acceptLanguage.
            cookie: (string): Custom cookie.
            crawlDelay: (number): Wait this many seconds between each URL crawled from a single IP address. Specify the number of seconds as an integer or floating-point number (e.g., crawlDelay=0.25).
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            maxHops: (integer): Limit the depth of your crawl.
            maxRounds: (integer): Specify the maximum number of crawl repeats. By default (maxRounds=0) repeating crawls will continue indefinitely.
            maxToCrawl: (integer): Specify max pages to spider. Default: 100,000.
            maxToProcess: (integer): Specify max pages to process through "processPagesVia" parameter. Default: 100,000.
            obeyRobots: (boolean): Ignore a site's robots.txt instructions.
            onlyProcessIfNew: (boolean): By default repeat crawls will only process new (previously unprocessed) pages.
            pageProcessPatterns: (array): Specify strings to limit pages processed to those whose HTML contains any of the content strings.
            processPagesVia: (string): The Analyze API automatically identifies and extracts articles, images and product pages.
            referer: (string): Custom referer.
            repeat: (number): Specify the number of days as a floating-point (e.g. repeat=7.0) to repeat this crawl. By default crawls will not be repeated.
            restrictDomain: (boolean): Allow limited crawling across subdomains/domains.
            seed: (string): Seed URL.
            urlCrawlPatterns: (array): Only crawl URLs that contain any of these text strings (one per line).
            urlCrawlRegEx: (string): Only crawl URLs matching the regular expression..
            urlProcessPatterns: (array): Only process URLs that contain any of the text strings (one per line).
            urlProcessRegEx: (string): Only process URLs matching the regular expression.
            useCanonical: (boolean): Skip pages with a differing canonical URL.
            useProxies: (boolean): Force proxy IPs.
            userAgent: (string): Custom User Agent.

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'processPagesVia', 'seed', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/websiteCrawler/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def websiteCrawlerEdit(self, inputId, seed, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            acceptLanguage: (string): Custom acceptLanguage.
            cookie: (string): Custom cookie.
            crawlDelay: (number): Wait this many seconds between each URL crawled from a single IP address. Specify the number of seconds as an integer or floating-point number (e.g., crawlDelay=0.25).
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            maxHops: (integer): Limit the depth of your crawl.
            maxRounds: (integer): Specify the maximum number of crawl repeats. By default (maxRounds=0) repeating crawls will continue indefinitely.
            maxToCrawl: (integer): Specify max pages to spider. Default: 100,000.
            maxToProcess: (integer): Specify max pages to process through "processPagesVia" parameter. Default: 100,000.
            obeyRobots: (boolean): Ignore a site's robots.txt instructions.
            onlyProcessIfNew: (boolean): By default repeat crawls will only process new (previously unprocessed) pages.
            pageProcessPatterns: (array): Specify strings to limit pages processed to those whose HTML contains any of the content strings.
            processPagesVia: (string): The Analyze API automatically identifies and extracts articles, images and product pages.
            referer: (string): Custom referer.
            repeat: (number): Specify the number of days as a floating-point (e.g. repeat=7.0) to repeat this crawl. By default crawls will not be repeated.
            restrictDomain: (boolean): Allow limited crawling across subdomains/domains.
            seed: (string): Seed URL.
            urlCrawlPatterns: (array): Only crawl URLs that contain any of these text strings (one per line).
            urlCrawlRegEx: (string): Only crawl URLs matching the regular expression..
            urlProcessPatterns: (array): Only process URLs that contain any of the text strings (one per line).
            urlProcessRegEx: (string): Only process URLs matching the regular expression.
            useCanonical: (boolean): Skip pages with a differing canonical URL.
            useProxies: (boolean): Force proxy IPs.
            userAgent: (string): Custom User Agent.

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'seed', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/websiteCrawler/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def websiteCrawlerList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/websiteCrawler/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def websiteCrawlerParameters(self):
        """Return Website Crawler parameters."""

        api_parameters = None
        parameters_names_map = {}
        api = '/inputs/websiteCrawler/parameters'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def websiteCrawlerRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/websiteCrawler/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def websiteCrawlerRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/websiteCrawler/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def websiteCrawlerStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/websiteCrawler/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def wordpressAdd(self, inputName, inputsGroupId, wordpressBlogUrl, **kwargs):
        """Create new input for inputsGroup.

        Args:
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            wordpressBlogUrl: (string): WordPress blog URL

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'wordpressBlogUrl', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/wordpress/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def wordpressEdit(self, inputId, wordpressBlogUrl, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            wordpressBlogUrl: (string): WordPress blog URL

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'wordpressBlogUrl', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/wordpress/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def wordpressList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/wordpress/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def wordpressRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/wordpress/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def wordpressRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/wordpress/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def wordpressStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/wordpress/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def yelpBusinessReviewsAdd(self, inputName, inputsGroupId, yelpBusinessesSearchLocation, **kwargs):
        """Create new input for inputsGroup.

        Args:
            description: (string): description
            inputName: (string): Can be any text.
            inputsGroupId: (string): inputsGroupId
            isRunning: (boolean): isRunning
            yelpBusinessesSearchLocation: (string): Required if either latitude or longitude is not provided. Specifies the combination of "address, neighborhood, city, state or zip, optional country" to be used when searching for businesses.
            yelpBusinessesSearchMaxNumberOfBusinessesToFetch: (integer): Max number of businesses to fetch per businesses search
            yelpBusinessesSearchRadius: (integer): Optional. Search radius in meters. The max value is 40000 meters (25 miles).
            yelpBusinessesSearchTerm: (string): Optional. Search term (e.g. "food", "restaurants"). If term isnt included we search everything. The term keyword also accepts business names such as "Starbucks".

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputName', 'inputsGroupId', 'yelpBusinessesSearchLocation', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/yelpBusinessReviews/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def yelpBusinessReviewsEdit(self, inputId, yelpBusinessesSearchLocation, **kwargs):
        """Edit existing inputsGroup for project.

        Args:
            description: (string): description
            inputId: (string): inputId
            isRunning: (boolean): isRunning
            yelpBusinessesSearchLocation: (string): Required if either latitude or longitude is not provided. Specifies the combination of "address, neighborhood, city, state or zip, optional country" to be used when searching for businesses.
            yelpBusinessesSearchMaxNumberOfBusinessesToFetch: (integer): Max number of businesses to fetch per businesses search
            yelpBusinessesSearchRadius: (integer): Optional. Search radius in meters. The max value is 40000 meters (25 miles).
            yelpBusinessesSearchTerm: (string): Optional. Search term (e.g. "food", "restaurants"). If term isnt included we search everything. The term keyword also accepts business names such as "Starbucks".

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'yelpBusinessesSearchLocation', '**kwargs']
        parameters_names_map = {}
        api = '/inputs/yelpBusinessReviews/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def yelpBusinessReviewsList(self, inputsGroupId):
        """Return the list of inputs for inputsGroup.

        Args:
            inputsGroupId: (string): inputsGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputsGroupId']
        parameters_names_map = {}
        api = '/inputs/yelpBusinessReviews/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def yelpBusinessReviewsRemove(self, inputId):
        """Remove existing input.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/yelpBusinessReviews/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def yelpBusinessReviewsRetrieve(self, inputId):
        """Return the input with specified ID.

        Args:
            inputId: (string): inputId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId']
        parameters_names_map = {}
        api = '/inputs/yelpBusinessReviews/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def yelpBusinessReviewsStatus(self, inputId, isRunning):
        """Set existing input status for project.

        Args:
            inputId: (string): inputId
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputId', 'isRunning']
        parameters_names_map = {}
        api = '/inputs/yelpBusinessReviews/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
