from edison_feeds_api import EdisonBaseAPI


class loginController(EdisonBaseAPI):
    """Login form."""

    _controller_name = "loginController"

    def login(self, login, password):
        """Login entry point.

        Args:
            login: (string): login
            password: (string): password

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['login', 'password']
        parameters_names_map = {}
        api = '/login'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
