from edison_feeds_api import EdisonBaseAPI


class filtersController(EdisonBaseAPI):
    """Filters management."""

    _controller_name = "filtersController"

    def getFiltersTree(self, alertRuleId):
        """Get filters tree.

        Args:
            alertRuleId: (string): alertRuleId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alertRuleId']
        parameters_names_map = {}
        api = '/filters/getFiltersTree'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def removeFiltersTree(self, alertRuleId):
        """Remove filters tree.

        Args:
            alertRuleId: (string): alertRuleId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alertRuleId']
        parameters_names_map = {}
        api = '/filters/removeFiltersTree'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def setFiltersTree(self, alertRuleId, filtersTree):
        """Set filters tree.

        Args:
            alertRuleId: (string): alertRuleId
            filtersTree: (string): filtersTree

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alertRuleId', 'filtersTree']
        parameters_names_map = {}
        api = '/filters/setFiltersTree'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
