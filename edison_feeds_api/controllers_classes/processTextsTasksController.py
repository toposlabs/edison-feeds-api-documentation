from edison_feeds_api import EdisonBaseAPI


class processTextsTasksController(EdisonBaseAPI):
    """Process Texts Tasks Controller"""

    _controller_name = "processTextsTasksController"

    def submit(self, privacyMode, projectId, text, **kwargs):
        """

        Args:
            date: (integer): The number of seconds since January 1, 1970, 00:00:00 GMT.
            languageId: (string): empty - AutoDetect.
            logging: (boolean): logging
            office365EmailType: (string): office365EmailType
            office365EmailsIncludeMode: (string): office365EmailsIncludeMode
            office365Groups: (array): office365Groups
            office365MailFolder: (string): office365MailFolder
            privacyMode: (boolean): In this mode the processed text not saved.
            projectId: (string): projectId
            text: (string): text

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['privacyMode', 'projectId', 'text', '**kwargs']
        parameters_names_map = {}
        api = '/process-text-tasks/submit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
