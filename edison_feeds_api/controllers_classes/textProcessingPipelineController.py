from edison_feeds_api import EdisonBaseAPI


class textProcessingPipelineController(EdisonBaseAPI):
    """Text Processing Pipeline Controller"""

    _controller_name = "textProcessingPipelineController"

    def add(self, name, pipeline, **kwargs):
        """Create new text processing pipeline for authenticated user.

        Args:
            description: (string): description
            name: (string): name
            pipeline: (string): pipeline

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['name', 'pipeline', '**kwargs']
        parameters_names_map = {}
        api = '/textProcessingPipeline/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def default(self):
        """Returns default text processing pipeline."""

        api_parameters = None
        parameters_names_map = {}
        api = '/textProcessingPipeline/default'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, name, pipeline, textProcessingPipelineId, **kwargs):
        """Edit existing text processing pipeline by ID.

        Args:
            description: (string): description
            name: (string): name
            pipeline: (string): pipeline
            textProcessingPipelineId: (string): textProcessingPipelineId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['name', 'pipeline', 'textProcessingPipelineId', '**kwargs']
        parameters_names_map = {}
        api = '/textProcessingPipeline/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, **kwargs):
        """Return the list of text processing pipeline for authenticated user.

        Args:
            withPipeline: (boolean): withPipeline

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/textProcessingPipeline/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, textProcessingPipelineId):
        """Remove existing text processing pipeline for authenticated user.

        Args:
            textProcessingPipelineId: (string): textProcessingPipelineId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['textProcessingPipelineId']
        parameters_names_map = {}
        api = '/textProcessingPipeline/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, textProcessingPipelineId):
        """Return the text processing pipeline with specified ID.

        Args:
            textProcessingPipelineId: (string): textProcessingPipelineId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['textProcessingPipelineId']
        parameters_names_map = {}
        api = '/textProcessingPipeline/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
