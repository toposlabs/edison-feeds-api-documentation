from edison_feeds_api import EdisonBaseAPI


class emotionsController(EdisonBaseAPI):
    """Emotions"""

    _controller_name = "emotionsController"

    def list(self):
        """Return list of emotions."""

        api_parameters = None
        parameters_names_map = {}
        api = '/emotions/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, emotionId):
        """Retrieve existing emotion.

        Args:
            emotionId: (string): emotionId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionId']
        parameters_names_map = {}
        api = '/emotions/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
