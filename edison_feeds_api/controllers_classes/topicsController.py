from edison_feeds_api import EdisonBaseAPI


class topicsController(EdisonBaseAPI):
    """Topics Controller"""

    _controller_name = "topicsController"

    def list(self):
        """Return the list of topics."""

        api_parameters = None
        parameters_names_map = {}
        api = '/topics/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, id):
        """Return the topic with specified ID.

        Args:
            id: (string): id

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id']
        parameters_names_map = {}
        api = '/topics/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
