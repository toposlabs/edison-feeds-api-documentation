from edison_feeds_api import EdisonBaseAPI


class processFilesTasksController(EdisonBaseAPI):
    """Process Files Tasks Controller"""

    _controller_name = "processFilesTasksController"

    def submit(self, file, privacyMode, projectId, **kwargs):
        """

        Args:
            date: (type, format): The number of seconds since January 1, 1970, 00:00:00 GMT.
            file: (file): file
            languageId: (type): empty - AutoDetect.
            logging: (type): logging
            minRelevancy: (type, format): minRelevancy
            office365EmailType: (type): office365EmailType
            office365EmailsIncludeMode: (type): office365EmailsIncludeMode
            office365Groups: (type, items): office365Groups
            office365MailFolder: (type): office365MailFolder
            privacyMode: (type): In this mode the processed text not saved.
            projectId: (type): projectId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['file', 'privacyMode', 'projectId', '**kwargs']
        parameters_names_map = {}
        api = '/process-file-tasks/submit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
