from edison_feeds_api import EdisonBaseAPI


class logoutController(EdisonBaseAPI):
    """Logout."""

    _controller_name = "logoutController"

    def logout(self):
        """logout"""

        api_parameters = None
        parameters_names_map = {}
        api = '/logout'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
