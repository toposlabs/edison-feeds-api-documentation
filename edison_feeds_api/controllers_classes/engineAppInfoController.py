from edison_feeds_api import EdisonBaseAPI


class engineAppInfoController(EdisonBaseAPI):
    """Engine App Info Controller"""

    _controller_name = "engineAppInfoController"

    def retrieve(self):
        """Return EngineApp info."""

        api_parameters = None
        parameters_names_map = {}
        api = '/engine-app/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def searchParameters(self):
        """Return info."""

        api_parameters = None
        parameters_names_map = {}
        api = '/engine-app/searchParameters'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def system(self):
        """Return info."""

        api_parameters = None
        parameters_names_map = {}
        api = '/engine-app/system'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
