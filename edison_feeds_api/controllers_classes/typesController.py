from edison_feeds_api import EdisonBaseAPI


class typesController(EdisonBaseAPI):
    """Types Controller"""

    _controller_name = "typesController"

    def list(self, **kwargs):
        """Return the list of types.

        Args:
            dictionaryId: (string): dictionaryId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/types/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, id):
        """Return the type with specified ID.

        Args:
            id: (string): id

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id']
        parameters_names_map = {}
        api = '/types/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
