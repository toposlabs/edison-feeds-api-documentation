from edison_feeds_api import EdisonBaseAPI


class countriesController(EdisonBaseAPI):
    """Countries Controller"""

    _controller_name = "countriesController"

    def list(self):
        """Return the list of countries"""

        api_parameters = None
        parameters_names_map = {}
        api = '/countries/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, id):
        """Return a country by ID.

        Args:
            id: (string): id

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id']
        parameters_names_map = {}
        api = '/countries/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
