from edison_feeds_api import EdisonBaseAPI


class alertersController(EdisonBaseAPI):
    """Alerters management."""

    _controller_name = "alertersController"

    def addEmailAlerter(self, alertRuleId, email, name, **kwargs):
        """Create new Email alerter for alert rule.

        Args:
            alertRuleId: (string): alertRuleId
            bcc: (array): bcc
            description: (string): description
            email: (array): email
            name: (string): name

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alertRuleId', 'email', 'name', '**kwargs']
        parameters_names_map = {}
        api = '/alerters/addEmailAlerter'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def alerterTypesList(self):
        """Return list of alerters types."""

        api_parameters = None
        parameters_names_map = {}
        api = '/alerters/alerterTypesList'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def editEmailAlerter(self, alerterId, **kwargs):
        """Edit Email alerter for alert rule.

        Args:
            alerterId: (string): alerterId
            bcc: (array): bcc
            description: (string): description
            email: (array): email

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alerterId', '**kwargs']
        parameters_names_map = {}
        api = '/alerters/editEmailAlerter'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, alertRuleId):
        """Return list of alerters for alertRule.

        Args:
            alertRuleId: (string): alertRuleId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alertRuleId']
        parameters_names_map = {}
        api = '/alerters/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, alerterId):
        """Remove existing alerter.

        Args:
            alerterId: (string): alerterId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alerterId']
        parameters_names_map = {}
        api = '/alerters/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, alerterId):
        """Return an alerter by ID.

        Args:
            alerterId: (string): alerterId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alerterId']
        parameters_names_map = {}
        api = '/alerters/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
