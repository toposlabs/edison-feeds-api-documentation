from edison_feeds_api import EdisonBaseAPI


class rulesController(EdisonBaseAPI):
    """Manager of all loaded rules"""

    _controller_name = "rulesController"

    def list(self, **kwargs):
        """Return the list of all rules.

        Args:
            locale: (string): locale

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/rules/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, ruleId, **kwargs):
        """Return the rule.

        Args:
            locale: (string): locale
            ruleId: (string): ruleId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['ruleId', '**kwargs']
        parameters_names_map = {}
        api = '/rules/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
