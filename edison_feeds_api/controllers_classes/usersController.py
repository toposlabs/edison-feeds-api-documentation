from edison_feeds_api import EdisonBaseAPI


class usersController(EdisonBaseAPI):
    """User - is someone who can edit data in the database.  All CRUD- operations possible to implement. """

    _controller_name = "usersController"

    def add(self, login, password, **kwargs):
        """Create new user.

        Args:
            description: (string): description
            enabled: (boolean): enabled
            login: (string): login
            password: (string): password

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['login', 'password', '**kwargs']
        parameters_names_map = {}
        api = '/users/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, id, **kwargs):
        """Edit existing user.

        Args:
            description: (string): description
            enabled: (boolean): enabled
            id: (string): id
            login: (string): login
            password: (string): password

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id', '**kwargs']
        parameters_names_map = {}
        api = '/users/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def editUserAttributes(self, **kwargs):
        """Edit user attributes.

        Args:
            dashboardsRefreshEvery: (string): dashboardsRefreshEvery
            dashboardsRefreshRate: (integer): dashboardsRefreshRate

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/users/editUserAttributes'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """Return the list of users"""

        api_parameters = None
        parameters_names_map = {}
        api = '/users/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, id):
        """Remove existing user.

        Args:
            id: (string): id

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id']
        parameters_names_map = {}
        api = '/users/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieveUserAttributes(self):
        """Return attributes of user's account."""

        api_parameters = None
        parameters_names_map = {}
        api = '/users/retrieveUserAttributes'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def whoAmI(self):
        """Return the user information."""

        api_parameters = None
        parameters_names_map = {}
        api = '/users/whoAmI'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
