from edison_feeds_api import EdisonBaseAPI


class doctypesController(EdisonBaseAPI):
    """Doctypes"""

    _controller_name = "doctypesController"

    def list(self):
        """Return list of doctypes."""

        api_parameters = None
        parameters_names_map = {}
        api = '/doctypes/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, doctypeId):
        """Retrieve existing doctype.

        Args:
            doctypeId: (string): doctypeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeId']
        parameters_names_map = {}
        api = '/doctypes/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
