from edison_feeds_api import EdisonBaseAPI


class alertRulesController(EdisonBaseAPI):
    """Alert rule defines a method of processing the data in Elasticsearch."""

    _controller_name = "alertRulesController"

    def add(self, alertRuleName, projectId, **kwargs):
        """Create new alert rule for project.

        Args:
            alertRuleName: (string): alertRuleName
            description: (string): description
            isRunning: (boolean): isRunning
            projectId: (string): projectId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alertRuleName', 'projectId', '**kwargs']
        parameters_names_map = {}
        api = '/alertRules/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, alertRuleId, isRunning, **kwargs):
        """Edit existing alert rule.

        Args:
            alertRuleId: (string): alertRuleId
            alertRuleName: (string): alertRuleName
            description: (string): description
            isRunning: (boolean): isRunning

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alertRuleId', 'isRunning', '**kwargs']
        parameters_names_map = {}
        api = '/alertRules/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, projectId):
        """Return list of alert rules for project.

        Args:
            projectId: (string): projectId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['projectId']
        parameters_names_map = {}
        api = '/alertRules/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, alertRuleId):
        """Remove existing alert rule.

        Args:
            alertRuleId: (string): alertRuleId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alertRuleId']
        parameters_names_map = {}
        api = '/alertRules/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, alertRuleId):
        """Return the alert rule by ID.

        Args:
            alertRuleId: (string): alertRuleId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alertRuleId']
        parameters_names_map = {}
        api = '/alertRules/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
