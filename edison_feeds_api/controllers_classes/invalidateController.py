from edison_feeds_api import EdisonBaseAPI


class invalidateController(EdisonBaseAPI):
    """Invalidate user's session forcefully."""

    _controller_name = "invalidateController"

    def invalidate(self, id):
        """Invalidate user's session forcefully.

        Args:
            id: (string): id

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id']
        parameters_names_map = {}
        api = '/invalidate'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
