from edison_feeds_api import EdisonBaseAPI


class entitiesController(EdisonBaseAPI):
    """Manager for choosing expected entities in search results for alerts."""

    _controller_name = "entitiesController"

    def geoRetrieve(self, entityId):
        """Return the entity with specified ID.

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/entities/geo/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def getGeoEntities(self, languageId, name, **kwargs):
        """Return JSON array of the objects which represent the entities with the requested name.

        Args:
            languageId: (string): languageId
            maxEntitiesNumber: (integer): maxEntitiesNumber
            name: (string): name
            onlyDirectChildren: (boolean): onlyDirectChildren
            onlyMainNames: (boolean): onlyMainNames
            parentEntityId: (string): parentEntityId
            sortByPopularity: (boolean): sortByPopularity
            wholeSubTree: (boolean): wholeSubTree

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', 'name', '**kwargs']
        parameters_names_map = {}
        api = '/entities/getGeoEntities'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def getTopicEntities(self, languageId, name, **kwargs):
        """Return JSON array of the objects which represent the entities with the requested name.

        Args:
            languageId: (string): languageId
            maxEntitiesNumber: (integer): maxEntitiesNumber
            name: (string): name
            onlyMainNames: (boolean): onlyMainNames
            sortByPopularity: (boolean): sortByPopularity
            topicTypeId: (string): topicTypeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', 'name', '**kwargs']
        parameters_names_map = {}
        api = '/entities/getTopicEntities'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def topicRetrieve(self, entityId):
        """Return the entity with specified ID.

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/entities/topic/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
