from edison_feeds_api import EdisonBaseAPI


class dashboardsController(EdisonBaseAPI):
    """Dashboards management for projects."""

    _controller_name = "dashboardsController"

    def list(self, projectId, **kwargs):
        """Return list of dashboards for selected project.

        Args:
            projectId: (string): projectId
            updateInterval: (string): updateInterval

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['projectId', '**kwargs']
        parameters_names_map = {}
        api = '/dashboards/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, dashboardId, **kwargs):
        """Return dashboard by ID.

        Args:
            dashboardId: (string): dashboardId
            updateInterval: (string): updateInterval

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['dashboardId', '**kwargs']
        parameters_names_map = {}
        api = '/dashboards/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
