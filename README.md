https://readthedocs.org/projects/edison-feeds-api-documentation/

1) copy over edison_feeds_api, package_templates, and source_feeds from swagger-api-parser project
2) sphinx-build -b html source_feeds _build
3) test from _build directory
4) remove _build
5) commit changes
6) goto https://readthedocs.org/projects/edison-feeds-api-documentation/
7) Click Build version button