.. edison_feeds_api documentation master file, created by
   sphinx-quickstart on Fri Apr 26 09:45:13 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to edison_feeds_api's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
